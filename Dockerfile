FROM crpdigitalprd0200.azurecr.io/openjdk:11-jre
LABEL maintainer="DevSecOps-Pacifico"
ENV spring.application.name ms-crypto-demo-v1
COPY build/libs/ms-crypto-demo-*SNAPSHOT.jar /opt/ms-crypto-demo.jar
ENTRYPOINT ["java", "-Djava.file.encoding=UTF-8","-jar","/opt/ms-crypto-demo.jar"]
