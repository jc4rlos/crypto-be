package pe.com.pacifico.kuntur.expose.response;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Class</b>: CryptoResponse <br/>
 * <b>Copyright</b>: 2023 Pacifico Seguros - La Chakra <br/>.
 *
 * @author 2023  Pacifico Seguros - La Chakra <br/>
 * <u>Service Provider</u>: Soluciones Digitales <br/>
 * <u>Developed by</u>: La Chakra developer <br/>
 * <u>Changes:</u><br/>
 * <ul>
 *   <li>
 *     April 21, 2023 Creación de Clase.
 *   </li>
 * </ul>
 */
@ApiModel(description = "CryptoResponse model")
@AllArgsConstructor
@Builder
@Getter
@NoArgsConstructor
@Setter
@ToString
public class CryptoResponse {

  private String plaintTextEncrypted;
}
