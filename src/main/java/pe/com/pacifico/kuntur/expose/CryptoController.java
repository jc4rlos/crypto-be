package pe.com.pacifico.kuntur.expose;

import io.swagger.annotations.ApiOperation;;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.pacifico.kuntur.business.CryptoService;
import pe.com.pacifico.kuntur.expose.request.CryptoRequest;
import pe.com.pacifico.kuntur.expose.response.CryptoResponse;
import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: CryptoController <br/>
 * <b>Copyright</b>: 2023 Pacifico Seguros - La Chakra <br/>.
 *
 * @author 2023  Pacifico Seguros - La Chakra <br/>
 * <u>Service Provider</u>: Soluciones Digitales <br/>
 * <u>Developed by</u>: La Chakra developer <br/>
 * <u>Changes:</u><br/>
 * <ul>
 *   <li>
 *     April 21, 2023 Creación de Clase.
 *   </li>
 * </ul>
 */
@RestController
@RequestMapping("/crypto")
@Slf4j
@RequiredArgsConstructor
public class CryptoController {

  private final CryptoService cryptoService;

  /**
   * This method is used to get only one cryptoToBeObfuscated.
   * @param request This is the first parameter to method.
   * @return one crypto.
   */
  @PostMapping
  @ApiOperation(value = "Crypto value", notes = "Endpoint example crypto", response = CryptoResponse.class)
  public Mono<CryptoResponse> encrypt(@RequestBody @Valid CryptoRequest request) {
    return cryptoService.encrypt(request);
  }

}
