package pe.com.pacifico.kuntur.business.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pe.com.pacifico.kuntur.business.CryptoService;
import pe.com.pacifico.kuntur.config.CryptoEnvironmentConfig;
import pe.com.pacifico.kuntur.expose.request.CryptoRequest;
import pe.com.pacifico.kuntur.expose.response.CryptoResponse;
import reactor.core.publisher.Mono;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * <b>Class</b>: CryptoServiceImpl <br/>
 * <b>Copyright</b>: 2023 Pacifico Seguros - La Chakra <br/>.
 *
 * @author 2023  Pacifico Seguros - La Chakra <br/>
 * <u>Service Provider</u>: Soluciones Digitales <br/>
 * <u>Developed by</u>: La Chakra developer <br/>
 * <u>Changes:</u><br/>
 * <ul>
 *   <li>
 *     April 21, 2023 Creación de Clase.
 *   </li>
 * </ul>
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CryptoServiceImpl implements CryptoService {

  private static final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";
  private final CryptoEnvironmentConfig cryptoEnvironment;


  public static String decrypt(final byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    Cipher cipher = Cipher.getInstance(TRANSFORMATION);
    cipher.init(Cipher.DECRYPT_MODE, privateKey);
    return new String(cipher.doFinal(data));
  }

  public static String decrypt(String data, String base64PrivateKey) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
    return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
  }

  private static PrivateKey getPrivateKey(final String base64PrivateKey) {
    PrivateKey privateKey = null;
    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
    KeyFactory keyFactory = null;
    try {
      keyFactory = KeyFactory.getInstance("RSA");
    } catch (NoSuchAlgorithmException exception) {
      log.error("error: {}", exception.getMessage());
    }
    try {
      assert keyFactory != null;
      privateKey = keyFactory.generatePrivate(keySpec);
    } catch (InvalidKeySpecException exception) {
      log.error("error: {}", exception.getMessage());
    }
    return privateKey;
  }

  @Override
  public Mono<CryptoResponse> encrypt(final CryptoRequest request) {
    try {
      final var encryptedText = encrypt(request.getPlainText(), cryptoEnvironment.getPublicKey());
      return Mono.just(new CryptoResponse(encryptedText));
    } catch (Exception e) {
      log.error("error; {}", e.getMessage());
    }
    return Mono.empty();
  }

  public String encrypt(final String content, final String publicKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    final var contentBytes = content.getBytes(StandardCharsets.UTF_8);
    final var cipher = Cipher.getInstance(TRANSFORMATION);
    cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
    final var cipherContent = cipher.doFinal(contentBytes);
    return Base64.getEncoder().encodeToString(cipherContent);
  }

  private PublicKey getPublicKey(final String base64PublicKey) {
    PublicKey publicKey;
    try {
      X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      publicKey = keyFactory.generatePublic(keySpec);
      return publicKey;
    } catch (NoSuchAlgorithmException | InvalidKeySpecException exception) {
      log.error("error getPublicKey: {}", exception.getMessage());
    }
    return null;
  }
}
