package pe.com.pacifico.kuntur.business;

import pe.com.pacifico.kuntur.expose.request.CryptoRequest;
import pe.com.pacifico.kuntur.expose.response.CryptoResponse;
import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: CryptoService <br/>
 * <b>Copyright</b>: 2023 Pacifico Seguros - La Chakra <br/>.
 *
 * @author 2023  Pacifico Seguros - La Chakra <br/>
 * <u>Service Provider</u>: Soluciones Digitales <br/>
 * <u>Developed by</u>: La Chakra developer <br/>
 * <u>Changes:</u><br/>
 * <ul>
 *   <li>
 *     April 21, 2023 Creación de Clase.
 *   </li>
 * </ul>
 */
public interface CryptoService {

  Mono<CryptoResponse> encrypt(CryptoRequest request);

}
