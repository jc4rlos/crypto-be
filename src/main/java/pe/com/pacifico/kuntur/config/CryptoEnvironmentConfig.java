package pe.com.pacifico.kuntur.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "crypto")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CryptoEnvironmentConfig {
  private String publicKey;
  private String privateKey;
}
